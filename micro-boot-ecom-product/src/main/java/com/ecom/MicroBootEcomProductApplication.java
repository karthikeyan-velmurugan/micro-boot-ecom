package com.ecom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

//@SpringBootApplication(exclude = {JpaRepositoriesAutoConfiguration.class,
//		HibernateJpaAutoConfiguration.class})
@SpringBootApplication
@EnableEurekaClient
public class MicroBootEcomProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroBootEcomProductApplication.class, args);
	}
}