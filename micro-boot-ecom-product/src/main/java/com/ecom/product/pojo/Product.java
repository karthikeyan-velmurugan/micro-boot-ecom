/**
 * 
 */
package com.ecom.product.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Karthikeyan.v
 *
 */
@Entity(name="tbl_product")
@JsonIgnoreProperties(value = {"createOn","updatedOn"}, allowGetters = true)
public class Product{

	@Id
	@GeneratedValue
	private Long productId;
	
	private String prodName;
	
	private Double price;
	
	private Integer quantity;	
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createOn;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	private Date updatedOn;
	
	private boolean isExist=true;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getCreateOn() {
		return createOn;
	}

	public void setCreateOn(Date createOn) {
		this.createOn = createOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isExist() {
		return isExist;
	}

	public void setExist(boolean isExist) {
		this.isExist = isExist;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", prodName=" + prodName + ", price=" + price + ", quantity="
				+ quantity + ", createOn=" + createOn + ", updatedOn=" + updatedOn + ", isExist=" + isExist + "]";
	}
}