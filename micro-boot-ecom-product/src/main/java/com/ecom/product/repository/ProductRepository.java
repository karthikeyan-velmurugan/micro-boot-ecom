/**
 * 
 */
package com.ecom.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ecom.product.pojo.Product;

/**
 * @author Karthikeyan.v
 *
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

}
