/**
 * 
 */
package com.ecom.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecom.product.pojo.Product;
import com.ecom.product.repository.ProductRepository;

/**
 * @author Karthikeyan.v
 *
 */
@RestController
@RequestMapping(path="/product")
public class ProductController {

	private final ProductRepository repository;

	@Autowired
	ProductController(ProductRepository repository){
		this.repository=repository;
	}

	@PostMapping
	public Product create(@RequestBody Product product){
		return repository.save(product);
	}

	@GetMapping
	public List<Product> findAll(){
		return repository.findAll();
	}

	@GetMapping(path = {"/{id}"})
	public ResponseEntity<Product> findById(@PathVariable long id){
		return repository.findById(id)
				.map(record -> ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}

	@PutMapping(value="/{id}")
	public ResponseEntity<Product> update(@PathVariable("id") long id,@RequestBody Product product){
		return repository.findById(id)
				.map(record -> {
					record.setProdName(product.getProdName());
					record.setPrice(product.getPrice());
					record.setQuantity(product.getQuantity());
					Product updated = repository.save(record);
					return ResponseEntity.ok().body(updated);
				}).orElse(ResponseEntity.notFound().build());
	}

	@DeleteMapping(path ={"/{id}"})
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		return repository.findById(id)
				.map(record -> {
					repository.deleteById(id);
					return ResponseEntity.ok().build();
				}).orElse(ResponseEntity.notFound().build());
	}
}