/**
 * 
 */
package com.ecom.profile.controller;

import java.util.HashSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecom.profile.domain.Address;
import com.ecom.profile.domain.Profile;
import com.ecom.profile.repository.ProfileRepository;

/**
 * @author Karthikeyan.v
 *
 */
@RestController
@RequestMapping(path="/profile")
public class ProfileController {

	private final ProfileRepository repository;

	@Autowired
	public ProfileController(ProfileRepository repository) {
		this.repository=repository;
	}

	@PostMapping
	public Profile create(@RequestBody Profile profile) {
		return repository.save(profile);
	}

	@GetMapping
	public List<Profile> findAll(){
		return repository.findAll();
	}

	@GetMapping(path= {"/{id}"})
	public ResponseEntity<Profile> findById(@PathVariable long id){
		return repository.findById(id)
				.map(record-> ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}

	@PostMapping(value="/{id}")
	public ResponseEntity<Profile> update(@PathVariable("id") long id, @RequestBody Profile profile){
		/**TODO Need to Add Functions**/
		Address address=new Address();
		HashSet<Address> addressSet=new HashSet<Address>();
		addressSet.add(address);

		return repository.findById(id).map(record->{
			record.setFistName(profile.getFistName());
			record.setLastName(profile.getLastName());
			record.setMobile(profile.getMobile());
			record.setMobVerified(profile.isMobVerified());
			record.setEmail(profile.getEmail());
			record.setEmailVerified(profile.isEmailVerified());
			record.setPassword(profile.getPassword());
			record.setExist(profile.isExist());
			record.setAddress(addressSet);
			Profile updated=repository.save(record);
			return ResponseEntity.ok().body(updated);
		}).orElse(ResponseEntity.notFound().build());
	}

	@DeleteMapping(path={"/{id}"})
	public ResponseEntity<?> delete(@PathVariable long id){
		return repository.findById(id)
				.map(record->{
					repository.deleteById(id);
					return ResponseEntity.ok().build();
				}).orElse(ResponseEntity.notFound().build());
	}

}