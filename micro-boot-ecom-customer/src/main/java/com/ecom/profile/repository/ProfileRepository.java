/**
 * 
 */
package com.ecom.profile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ecom.profile.domain.Profile;

/**
 * @author Karthikeyan.v
 *
 */
@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long>{

}
