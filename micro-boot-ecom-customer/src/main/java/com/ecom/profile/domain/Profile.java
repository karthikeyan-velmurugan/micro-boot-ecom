/**
 * 
 */
package com.ecom.profile.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Karthikeyan.v
 *
 */
@Entity(name="tbl_profile")
@JsonIgnoreProperties(value = {"createOn","updatedOn"}, allowGetters = true)
public class Profile{
	
	@Id
	@GeneratedValue
	private Long profieId;
	
	private String fistName;
	
	private String lastName;
	
	private Long mobile;
	
	private boolean isMobVerified=false;
	
	private String email;
	
	private boolean isEmailVerified=false;
	
	private String password;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createOn;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	private Date updatedOn;
	
	private boolean isExist=true;
	
	@OneToMany(mappedBy="profile", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Set<Address> address;

	public Long getProfieId() {
		return profieId;
	}

	public void setProfieId(Long profieId) {
		this.profieId = profieId;
	}

	public String getFistName() {
		return fistName;
	}

	public void setFistName(String fistName) {
		this.fistName = fistName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public boolean isMobVerified() {
		return isMobVerified;
	}

	public void setMobVerified(boolean isMobVerified) {
		this.isMobVerified = isMobVerified;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEmailVerified() {
		return isEmailVerified;
	}

	public void setEmailVerified(boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreateOn() {
		return createOn;
	}

	public void setCreateOn(Date createOn) {
		this.createOn = createOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isExist() {
		return isExist;
	}

	public void setExist(boolean isExist) {
		this.isExist = isExist;
	}

	public Set<Address> getAddress() {
		return address;
	}

	public void setAddress(Set<Address> address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Profile [profieId=" + profieId + ", fistName=" + fistName + ", lastName=" + lastName + ", mobile="
				+ mobile + ", isMobVerified=" + isMobVerified + ", email=" + email + ", isEmailVerified="
				+ isEmailVerified + ", password=" + password + ", createOn=" + createOn + ", updatedOn=" + updatedOn
				+ ", isExist=" + isExist + "]";
	}
}
