/**
 * 
 */
package com.ecom.profile.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Karthikeyan.v
 *
 */
@Entity(name="tbl_address")
@JsonIgnoreProperties(value= {"createOn","updatedOn"}, allowGetters = true)
public class Address{

	@Id
	@GeneratedValue
	private Long addressId;

	private String fullName;

	private Long mobile;

	private Long alterMobile;

	private Integer pincode;

	private String flatNoName; //(Flat/House No/Floor/Building)

	private String streetColonyName; //(Colony/Street/Locality)

	private String landmark;

	private String city;

	private String state;

	private String country;

	private String addessType; //(Home/Office)

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createOn;

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	private Date updatedOn;

	private boolean isExist=true;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="profileId", nullable=false)
	private Profile profile;

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public Long getAlterMobile() {
		return alterMobile;
	}

	public void setAlterMobile(Long alterMobile) {
		this.alterMobile = alterMobile;
	}

	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	public String getFlatNoName() {
		return flatNoName;
	}

	public void setFlatNoName(String flatNoName) {
		this.flatNoName = flatNoName;
	}

	public String getStreetColonyName() {
		return streetColonyName;
	}

	public void setStreetColonyName(String streetColonyName) {
		this.streetColonyName = streetColonyName;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddessType() {
		return addessType;
	}

	public void setAddessType(String addessType) {
		this.addessType = addessType;
	}

	public Date getCreateOn() {
		return createOn;
	}

	public void setCreateOn(Date createOn) {
		this.createOn = createOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isExist() {
		return isExist;
	}

	public void setExist(boolean isExist) {
		this.isExist = isExist;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	@Override
	public String toString() {
		return "Address [addressId=" + addressId + ", fullName=" + fullName + ", mobile=" + mobile + ", alterMobile="
				+ alterMobile + ", pincode=" + pincode + ", flatNoName=" + flatNoName + ", streetColonyName="
				+ streetColonyName + ", landmark=" + landmark + ", city=" + city + ", state=" + state + ", country="
				+ country + ", addessType=" + addessType + ", createOn=" + createOn + ", updatedOn=" + updatedOn
				+ ", isExist=" + isExist + "]";
	}		
}