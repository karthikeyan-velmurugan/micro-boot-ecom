package com.ecom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MicroBootEcomOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroBootEcomOrderApplication.class, args);
	}
}