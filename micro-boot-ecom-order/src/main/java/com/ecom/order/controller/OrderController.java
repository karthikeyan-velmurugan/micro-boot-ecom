/**
 * 
 */
package com.ecom.order.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecom.order.pojo.Order;
import com.ecom.order.repository.OrderRepository;

/**
 * @author Karthikeyan.v
 *
 */
@RestController
@RequestMapping(path="/order")
public class OrderController {

	private final OrderRepository repository;

	@Autowired
	public OrderController(OrderRepository repository) {
		this.repository=repository;
	}

	@PostMapping
	public Order create(@RequestBody Order order) {
		return repository.save(order);
	}

	@GetMapping
	public List<Order> findAll(){
		return repository.findAll();
	}

	@GetMapping(path= {"/{id}"})
	public ResponseEntity<Order> findById(@PathVariable long id){
		return repository.findById(id)
				.map(record->ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}
	
	/*@PostMapping(value="/{id}")
	public ResponseEntity<Order> update(@PathVariable("id") long id,@RequestBody Order order)
	{
		
	}*/
}