/**
 * 
 */
package com.ecom.order.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Karthikeyan.v
 *
 */
@Entity(name="tbl_order")
@JsonIgnoreProperties(value={"createdOn","updtaedOn"},allowGetters=true)
public class Order {

	@Id
	@GeneratedValue
	private Long orderId;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date createdOn;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	private Date updtaedOn;

	private boolean isExist=true;

}
