/**
 * 
 */
package com.ecom.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ecom.order.pojo.Order;

/**
 * @author Karthikeyan.v
 *
 */
@Repository
public interface OrderRepository extends JpaRepository<Order,Long>{

}
