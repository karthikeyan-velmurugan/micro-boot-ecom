package com.ecom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class MicroBootEcomServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroBootEcomServerApplication.class, args);
	}
}